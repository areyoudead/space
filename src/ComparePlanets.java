import java.util.Scanner;

public class ComparePlanets {

    // Задаём константой (final) количество планет
    public static final int PLANETS_COUNT = 5;


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // Задаём массив планет (5 штук)
        Planet[] planets = new Planet[PLANETS_COUNT];

        // Заполняем массив
        for (int i = 0; i < PLANETS_COUNT; i++) {

            // В этой строке обязательны скобочки (i + 1), так как сначала происходит сложение чисел,
            // потом преобразование в строку и конкатенация строк. Если убрать скобки,
            // сначала будет преобразование в строки и их конкатенации.
            // То есть, при i = 2, например, будет "Enter 21 planet's name: "
            System.out.println("Enter " + (i + 1) + " planet's name: ");

            // Считываем строку (имя планеты) с помощью сканнера
            String planetName = scanner.next();

            // Создаём новый экземпляр планеты, передаём имя в конструктор
            // и присваиваем i-му элементу массива
            planets[i] = new Planet(planetName);
        }

        // Находим максимальный радиус
        double maxRadius = 0;

        for (int i = 0; i < PLANETS_COUNT; i++) {

            // Берём радиус i-ой планеты
            double radius = planets[i].getRadius();

            if (radius > maxRadius) {

                maxRadius = radius;
            }
        }

        System.out.println("Max radius: " + maxRadius);
    }


}