public class Planet {

	// Константа (final) - множитель для рандома
	// (в других классах она явно не нужна)
	private static final int SEED = 5654;

	// private делает недоступными эти поля
	// при обращении из других классов
	private String name;
	private double radius;

	/**
	 * Конструктор
	 * Передаём имя планеты,
	 * а радиус будет задаваться случайным образом
	 * @param name имя планеты
	 */
	public Planet(String name) {
		this.name = name;
		this.radius = Math.random() * SEED;
	}

	// создаём стандартные методы - геттеры, для чтения полей
	public String getName() {
		return name;
	}

	public double getRadius() {
		return radius;
	}
}
